package gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import javax.swing.JTextField;

public class Twizzy extends JFrame {
	int taux=80;
	String url="video2.avi";

	private JPanel jPanel1;


	private DaemonThread myThread = null;
	int count = 0;
	VideoCapture webSource = null;

	Mat frame = new Mat();
	MatOfByte mem = new MatOfByte();
	private JTextField txtPanneauDetect;
	private JTextField txtMatching;

	//*classe de thread de d�tection//*
	
	class DaemonThread implements Runnable
	{
		protected volatile boolean runnable = false;

		@Override
		public  void run()
		{
			synchronized(this)
			{
				while(runnable)
				{
					if(webSource.grab())
					{
						try
						{
							webSource.retrieve(frame);//extraction de l'image de la vid�o

							//*Traitement*//

							Mat destination = new Mat(frame.rows(),frame.cols(), frame.type());
							//reglage luminosite
							frame.convertTo(destination, -1, 1, 0);
							Highgui.imwrite("brightWithAlpha2Beta50.jpg", destination);
							Mat mat=LectureImage("brightWithAlpha2Beta50.jpg");
							
							//Lecture et conversion du Frame en HSV
							Mat hsv_image = Mat.zeros(mat.size(), mat.type());
							Imgproc.cvtColor(mat, hsv_image, Imgproc.COLOR_BGR2HSV);

							//Seuillage
							Mat threshold_img = DetecterCercles(hsv_image);
							
							//Tableau des contours circulaires d�tect�s 
							ArrayList<MatOfPoint> contours=DetecterContours(threshold_img);

							//Tableau de matrices des panneaux detect�s
							ArrayList<Mat> panneaux = new ArrayList<Mat>();
							panneaux=DetecterPanneaux(contours,mat);
							//Thread.sleep(33);


							ArrayList<Mat> References=new ArrayList<Mat>();
							//*Matrices des panneaux de r�f�rence
							Mat mat_ref30= Highgui.imread("ref30.JPG",Highgui.CV_LOAD_IMAGE_COLOR);
							Mat mat_ref50= Highgui.imread("ref50.JPG",Highgui.CV_LOAD_IMAGE_COLOR);
							Mat mat_ref70= Highgui.imread("ref70.JPG",Highgui.CV_LOAD_IMAGE_COLOR);
							Mat mat_ref90= Highgui.imread("ref90.JPG",Highgui.CV_LOAD_IMAGE_COLOR);
							Mat mat_ref110= Highgui.imread("ref110.JPG",Highgui.CV_LOAD_IMAGE_COLOR);

							References.add(mat_ref30);
							References.add(mat_ref50);
							References.add(mat_ref70);
							References.add(mat_ref90);
							References.add(mat_ref110);

							Mat mat_ref=References.get(0);
							
							//parcours des diff�rents panneaux d�tect�s
							for (int c=0; c<panneaux.size();c++){
								ArrayList<Integer> Max=new ArrayList<Integer>();
								Mat hsv_panneau = Mat.zeros(panneaux.get(c).size(), panneaux.get(c).type());
								
								//conversion en domaine de gris
								Imgproc.cvtColor(panneaux.get(c), hsv_panneau, Imgproc.COLOR_BGR2GRAY);
								Mat A=new Mat();
								
								//Mise � l'�chelle
								Imgproc.resize( hsv_panneau, A, mat_ref.size() ); 
								
								//conversion en noir et blanc (que 255 et 0)
								Imgproc.threshold(A, A, 130, 255, Imgproc.THRESH_BINARY);
								
								//�limination du fond du panneaux d�tect� (=>am�lioration du matching)
								Point center= new Point(127,127);
								Mat A1=new Mat();
								A1=A.clone();
								Core.circle(A1, center,90, new Scalar(0,0,0),-1);
								Core.subtract(A, A1, A);
								Mat C=new Mat();
								Mat D=new Mat();
								
								//parcours des diff�rents panneaux de r�f�rence
								for(int i = 0 ; i < References.size(); i++){
									mat_ref=(References.get(i)); 
									Mat hsv_ref = Mat.zeros(mat_ref.size(), mat_ref.type());
									Imgproc.cvtColor(mat_ref, hsv_ref, Imgproc.COLOR_BGR2HSV);
									
									//Seuillage
									Mat threshold_ref = DetecterCerclesPrime(hsv_ref);
									Mat B=new Mat();
									
									//mise � l'�chelle
									Imgproc.resize( threshold_ref, B, mat_ref.size() ); 
									
									//conversion en noir et blanc
									Imgproc.threshold(B, B, 170, 255, Imgproc.THRESH_BINARY);
									
									//elimination du fond
									Mat B1=new Mat();
									B1=B.clone();
									Core.circle(B1, center,80, new Scalar(0,0,0),-1);
									Core.subtract(B, B1, B);
									
									//superposition des images
									Core.bitwise_xor(B, A, C);
									Core.bitwise_not(C, D); 

									//Affichage du panneau noir et blanc
									Highgui.imencode(".bmp", A, mem);
									Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
									BufferedImage buff = (BufferedImage) im;
									Graphics g=jPanel1.getGraphics();
									g.drawImage(buff, getWidth()-160, 200, getWidth()-60, 300 , 0, 0, buff.getWidth(), buff.getHeight(), null);


									//calcul de la concentration en blanc
									int concentration=Core.countNonZero(D);
									int t=254*254;
									double h = ( (double) concentration / (double) t )*100 ;
									Max.add((int) h);
									//System.out.println("matching du panneau "+(c+1)+" avec le panneau ref "+(i+1)+" ="+h+" %");
								} 
								
								//d�termination du panneau avec le plus de correspondance
								int max=0;
								int P=0;
								if (!Max.isEmpty()){
									max=Collections.max(Max);
									P=Max.indexOf(max);
									
									//condition sur la concentration en blanc
									if (max>taux){
										
										//affichage du panneau de r�f�rence correspondant
										Highgui.imencode(".bmp", References.get(P), mem);
										Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
										BufferedImage buff = (BufferedImage) im;
										Graphics g=jPanel1.getGraphics();
										g.drawImage(buff, getWidth()-160, 60, getWidth()-60, 160 , 0, 0, buff.getWidth(), buff.getHeight(), null);



									
										if ((P==0)){
											System.out.println("L'image correspondante est le panneau 30");
										}
										else if (P==1){
											System.out.println("L'image correspondante est le panneau 50");
										}
										else if (P==2){
											System.out.println("L'image correspondante est le panneau 70");
										}
										else if (P==3){
											System.out.println("L'image correspondante est le panneau 90");
										}
										else if (P==4){
											System.out.println("L'image correspondante est le panneau 110");
										}
									}
								}
							}

							//*Fin traitement*//


							//affichage de la vid�o
							Highgui.imencode(".bmp", mat, mem);
							Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
							BufferedImage buff = (BufferedImage) im;
							Graphics g=jPanel1.getGraphics();
							if (g.drawImage(buff, 15, 60, getWidth()-240, getHeight()-100 , 0, 0, buff.getWidth(), buff.getHeight(), null))
								if(runnable == false)
								{
									System.out.println("Going to wait()");
									this.wait();
								}
						}
						catch(Exception ex)
						{
							System.out.println("Error");
						}
					}
				}
			}
		}
	}



	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME); 
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Twizzy frame = new Twizzy();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	//interface
	/**
	 * Interface Graphique
	 */
	public Twizzy() {
		setMinimumSize(new Dimension(860, 600));
		setSize(new Dimension(860, 720));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 753, 476);
		jPanel1 = new JPanel();
		jPanel1.setMinimumSize(new Dimension(1080, 720));
		jPanel1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jPanel1);
		jPanel1.setLayout(null);

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(320, 260));
		panel.setBounds(10, 171, 401, 283);
		jPanel1.add(panel);

		final JButton jButton1 = new JButton("Start");
		final JButton jButton2 = new JButton("Stop");
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.loadLibrary("opencv_ffmpeg2413_64");
				//webSource= new VideoCapture(url);
				webSource =new VideoCapture(0);
				myThread = new DaemonThread();
				Thread t = new Thread(myThread);
				t.setDaemon(true);
				myThread.runnable = true;
				t.start();
				jButton1.setEnabled(false);  //start button
				jButton2.setEnabled(true);  // stop button

			}
		});
		jButton1.setBounds(97, 508, 89, 23);
		jPanel1.add(jButton1);


		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				myThread.runnable = false;
				jButton2.setEnabled(false);   
				jButton1.setEnabled(true);

				webSource.release();	

			}
		});
		jButton2.setBounds(379, 508, 89, 23);
		jPanel1.add(jButton2);

		JPanel panneau = new JPanel();
		panneau.setBounds(458, 120, 135, 121);
		jPanel1.add(panneau);

		txtPanneauDetect = new JTextField();
		txtPanneauDetect.setText("Panneau d\u00E9tect\u00E9");
		txtPanneauDetect.setBounds(699, 35, 105, 20);
		jPanel1.add(txtPanneauDetect);
		txtPanneauDetect.setColumns(10);

		txtMatching = new JTextField();
		txtMatching.setText("Matching");
		txtMatching.setColumns(10);
		txtMatching.setBounds(699, 171, 105, 20);
		jPanel1.add(txtMatching);
	}


/**
 * Lire une image
 * @param fichier String
 * @return Mat
 */

	public static Mat LectureImage(String fichier){
		File f=new File(fichier);
		Mat m = Highgui.imread(f.getAbsolutePath());
		return m;
	}

//Seuillage sur le rouge
	/**
	 * Seuillage du rouge
	 * @param hsv_image Mat
	 * @return org.opencv.core.Mat
	 */
	public static Mat DetecterCercles(Mat hsv_image){
		Mat threshold_img1 = new Mat();
		Mat threshold_img2 = new Mat();
		Mat threshold_img = new Mat();
		Core.inRange(hsv_image, new Scalar(0,102,0), new Scalar(10,255,255), threshold_img1);
		Core.inRange(hsv_image, new Scalar(160,70,0), new Scalar(180,255,255), threshold_img2);
		Core.bitwise_or(threshold_img1, threshold_img2, threshold_img);
		Imgproc.GaussianBlur(threshold_img, threshold_img, new Size(9,9), 2, 2);
		return threshold_img;
	}

//Seuillage sur le rouge et le noir
	/**
	 * Seuillage du rouge et du noir
	 * @param hsv_image Mat
	 * @return Mat
	 */
	public static Mat DetecterCerclesPrime(Mat hsv_image){
		Mat threshold_img1 = new Mat();
		Mat threshold_img2 = new Mat();
		Mat threshold_img3 = new Mat();
		Mat threshold_img4 = new Mat();
		Mat threshold_img5 = new Mat();
		Core.inRange(hsv_image, new Scalar(0,102,0), new Scalar(10,255,255), threshold_img1);
		Core.inRange(hsv_image, new Scalar(160,70,0), new Scalar(180,255,255), threshold_img2);
		Core.inRange(hsv_image, new Scalar(0,0,120), new Scalar(255,80,255), threshold_img3);
		Core.bitwise_or(threshold_img1, threshold_img2, threshold_img4);
		Core.bitwise_or(threshold_img3, threshold_img4, threshold_img5);

		Imgproc.GaussianBlur(threshold_img5, threshold_img5, new Size(9,9), 2,2);
		return threshold_img5;
	}

//d�tection des contours circulaires
	/**
	 * Detecter les contours � partir d'une image seuill�
	 * @param img Mat
	 * @return java.lang.reflect.Array
	 */
	public static ArrayList<MatOfPoint> DetecterContours (Mat img){
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		int thresh=100;
		Mat canny_output = new Mat();
		ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		MatOfInt4 hierarchy=new MatOfInt4();
		Imgproc.Canny(img, canny_output, thresh, thresh*2);
		Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
		Random rand = new Random();
		for (int i=0;i<contours.size();i++){
			Scalar color = new Scalar ( rand.nextInt(255-0+1), rand.nextInt(255-0+1), rand.nextInt(255-0+1));
			Imgproc.drawContours(drawing, contours, i, color, 1,8,hierarchy,0,new Point());

		}
		return contours;

	}
	
	
//retourne liste des matrices des panneaux detect�s
	/**
	 * Detecter les panneaux � partir des contours
	 * @param contours java.lang.reflect.Array
	 * @param mat org.opencv.core.Mat
	 * @return java.lang.reflect.Array
	 */
	public static ArrayList<Mat> DetecterPanneaux (ArrayList<MatOfPoint> contours, Mat mat){
		ArrayList<Mat> panneaux = new ArrayList<Mat>();
		MatOfPoint2f matOfPoint2f= new MatOfPoint2f();
		float[] radius = new float [1];
		Point center = new Point();
		for (int c=0; c<contours.size();c++){
			MatOfPoint contour=contours.get(c);
			double contourArea=Imgproc.contourArea(contour);
			matOfPoint2f.fromList(contour.toList());
			Imgproc.minAreaRect(matOfPoint2f);
			Imgproc.minEnclosingCircle(matOfPoint2f, center, radius);
			Rect rect=Imgproc.boundingRect(contour);
			if ((contourArea/(Math.PI*radius[0]*radius[0]))>=0.8){	
				Core.circle(mat, center,(int) radius[0], new Scalar(0,255,0),1);

				Core.rectangle(mat, new Point(rect.x,rect.y),
						new Point(rect.x+rect.width, rect.y+rect.height), 
						new Scalar (0,255,0),1);
				Mat tmp=mat.submat(rect.y,rect.y+rect.height,rect.x,rect.x+rect.width);
				Mat ball=Mat.zeros(tmp.size(), tmp.type());
				panneaux.add(ball);
				tmp.copyTo(ball);
			}
		}
		return panneaux;
	}
	
/**
 * Convertir une matrice en BufferedImage
 * @param m Mat
 * @return BufferedImage
 */
	public static BufferedImage Mat2BufferedImage(Mat m){
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if ( m.channels() > 1 ) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels()*m.cols()*m.rows();
		byte [] b = new byte[bufferSize];
		m.get(0,0,b); // get all the pixels
		BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);  
		return image;
	}
	
	
}
